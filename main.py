# -*- encoding:utf-8 -*-
import requests as r
import re
import urllib
import json
import sys

def Url():
	return 'http://ws.audioscrobbler.com/2.0/'

def KEY():
	f = open('APIkey','r')
	key = f.read()
	f.close()
	return key

def params(req):
	fm = {
	'method':req,
	'api_key':KEY().strip(),
	'format':'json'}
	return fm

def GetUserInfo(user):
	data = {
	'method':'user.GetUserInfoo',
	'user':user,
	'api_key':KEY(),
	'format':'json'}
	p = r.get(Url(),params=data)
	return p.text

def GetRecentTracks(user):
	l = []
	data = {
	'method':'user.getrecenttracks',
	'user':user,
	'limit':200,
	'api_key':KEY(),
	'format':'json'	}
	e = []
	for nk in range(1,6):
		data.update({'page':nk})
		try:
			p = r.get(Url(),params=data)
			e.append(json.loads(p.text))
		except Exception, e:
			#print str(e)
			pass
		"""
		for p in e:
			for t in p['recenttracks']['track']:
				l.append(t['date']['#text'].split(','))
		"""
	for p in e:
		for t in p['recenttracks']['track']:
			l.append(t['date']['#text'].split(','))
	return l

def ShowHourGraph(user):
	d = {}
	for i in range(1,25):
		d[i] = 0
	l = GetRecentTracks(user)
	for i in l:
		try:
			word = int(i[1][1]+i[1][2])
			d[word] += 1
			pass
		except Exception, e:
			#print str(e)
			pass
		
	j = '■'
	print "Based on your last %d scrobbles, your listening graph is like" % len(l)
	for i in range(1,25):
		sys.stdout.write(str(i)+'-'+str(i+1))
		for q in range(0,d[i]):
			sys.stdout.write(j)
		sys.stdout.write(' '+str(d[i])+'\n')


print ShowHourGraph	('pravj')